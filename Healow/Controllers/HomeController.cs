﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Healow.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public FileResult List()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/files/schedule.txt"));
            return File(fileBytes, "text/plain");
        }

        public FileResult Slots()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/files/Slots.txt"));
            return File(fileBytes, "text/plain");
        }

        


    }
}